import React, {useEffect, useState} from 'react';
import {useRouter} from "next/router";
import {css} from'@emotion/react'
import Image from 'next/image'
import bgImg1 from '../images/burgers/burgers-1.png'
import bgImg2 from '../images/burgers/burgers-2.png'
import bgImg3 from '../images/burgers/burgers-3.png'
import bgImg4 from '../images/burgers/burgers-4.png'
import bgImg5 from '../images/burgers/burgers-5.png'
import Burger from "../components/burger";
import Desk from "../components/desk";
import Modal from "../components/modal";


const GamePage = () => {
    const router =useRouter();
    const given = router.query
    const bgImg = [bgImg1,bgImg2,bgImg3,bgImg4,bgImg5]
    const [isVictory,setIsVictory] = useState(0)
    const [list,setList]=useState<number[]>([])
    const value = parseInt(given.value as string)
    const trueList=[...list]
    trueList.sort(function(a, b){return a - b});


    useEffect(()=>{
        const l:number[] = []
        while (l.length !== parseInt(given.amount as string)){
            let rand = Math.round(Math.random() * value);
            if(!l.includes(rand)){
                l.push(rand)
            }
        }
        setList(l)
    },[])

    // styles
    const styleContent = css`
        height: 100vh;
    `

    const styleBoard = css`
      width: 900px;
      height: 230px;
      margin: auto;
      margin-bottom: 50px;
      display: flex;
      justify-content: space-around;
      align-items: center;
    `

    return (
        <div className='bg-imageb' css={[styleContent,css`display: flex;flex-direction: column;justify-content: space-between;align-items: center`]}>
            <div css={css`display: flex;width: ${200*list.length}px;justify-content: space-between;padding-top: 80px;`}>
                {list.map((img,index)=>(
                    <Burger
                        key={index}
                        id={`burger-${index}`}
                        draggable={true}
                    >
                        <Image id={`imagine${index}`}
                               src={bgImg[index]}
                               alt="Picture of the author"
                               width={150}
                               height={150}
                               css={css`pointer-events: none`}
                        />
                        <span css={css`position: absolute;top: 55px;right: 105px;color: white;font-size: 25px`}>{list[index]}</span>
                    </Burger>
                ))}
            </div>
            <div>
                    <span css={css`font-size: 40px`}>По возрастанию</span>
            <div css={styleBoard} className='bg-burgers'>
                {list.map((img, index)=>(
                        <Desk key={index} id={`desk-${index}`} trueNum={trueList[index]} setNum={setIsVictory} num={isVictory}/>
                ))}
            </div>
            </div>
            {isVictory === trueList.length?<Modal/>:<></>}
        </div>
    );
};

export default GamePage;
