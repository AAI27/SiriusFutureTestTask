import React, {DragEvent} from 'react';
import {css} from "@emotion/react";

export interface IDesk{
    id:string
    trueNum:number
    num:number
    setNum:(e:number)=>void
}


const Desk = (props:IDesk) => {
    const drop = (e: DragEvent<HTMLDivElement>) =>{
        e.preventDefault();
        const burger_id = e.dataTransfer.getData('burger_id')
        const burger = document.getElementById(burger_id)

        if(parseInt(burger!.children[1].innerHTML) === props.trueNum){
            props.setNum(props.num+1)
            burger!.style.display = 'block' as string
            (e.target as HTMLDivElement).appendChild(burger as HTMLDivElement)
        }

    }

    const dragOver = (e:any) => {
        e.preventDefault();
    }
    const styleCircle = css`
      background-color: transparent;
      border-radius: 50%;
      width: 150px;
      height: 150px;
      box-shadow: inset 0px 4px 25px rgba(0, 0, 0, 0.25);
    `
    return (
        <div
            className='bg-burgers'
            id={props.id}
            onDrop={drop}
            onDragOver={dragOver}
            css={styleCircle}
        >
        </div>
    );
};

export default Desk;
