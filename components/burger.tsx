import React, {ReactNode} from 'react';
import {css} from "@emotion/react";

import bgImg1 from '../images/burgers/burgers-1.png'

export interface IBurger{
    id:string
    draggable:boolean
    children:ReactNode
}

const Burger = (props:IBurger) => {


    const dragStart = (e:any) =>{
        e.stopPropagation()
        const target = e.target;
        e.dataTransfer.setData('burger_id',target.id)
    }

    const dragOver = (e:any) =>{
        e.stopPropagation();
    }
    return (
        <div
            id={props.id}
            onDragStart={dragStart}
            draggable={props.draggable}
            onDragOver={dragOver}
            css={css`background: transparent;position: relative; margin: auto`}
        >
            {props.children}
        </div>
    );
};

export default Burger;
