import React from 'react';
import {css} from "@emotion/react";
import Link from "next/link";

const Modal = () => {
    const styleContent = css`
      background: rgba(32, 21, 54, 0.6);
      position: fixed;
      height: 100vw;
      padding-top: 100px;
      width: 100vw;
    `
    const styleContainer = css`
      text-align: center;
      display: flex;
      border: 10px solid darkgreen;
      box-shadow: 0px 4px 20px rgba(0, 0, 0, 0.25);
      border-radius: 15px;
      flex-direction: column;
      justify-content: space-around;
      background-color: white;
      width: 500px;
      height: 500px;
      margin: auto;
      z-index: 1;
    `
    const styleBtnPlay = css`
      background-color: #38DF7A;
      border-radius: 10px;
      border:none;
      font-size: 20px;
      height:30px;
      width: 170px;
      color: white;
      cursor: pointer;
      margin-bottom: 30px;
    `
    return (
        <div css={styleContent}>
            <div css={styleContainer}>
                <h1 css={css`margin:0;font-size: 80px;font-family: Arial, Helvetica, sans-serif;background: linear-gradient(to bottom,rgba(255, 249, 216, 1),rgba(255, 228, 79, 1));-webkit-text-fill-color: transparent;-webkit-background-clip: text;`}>Победа!</h1>
                <p css={css`margin:0;font-size: 40px`}>Молодец! Ты успешно справился с заданием!</p>
                <Link href='/'>
                    <button css={styleBtnPlay}>Заново</button>
                </Link>
            </div>
        </div>
    );
};

export default Modal;
